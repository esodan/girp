/* girp-method-common.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
using GXml;

public class Girp.MethodCommon : Element, Named, Documented, Member, MappeableElement
{
  [Description (nick="::name")]
  public string name { get; set; }
  [Description (nick="::c:identifier")]
  public string cidentifier { get; set; }
  [Description (nick="::version")]
  public string version { get; set; }
  public ReturnValue return_value { get; set; }
  public Doc doc { get; set; }

  construct {
    parse_children = false;
  }

  public string get_map_key () { return name; }
}
