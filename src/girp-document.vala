/* girp-repository.vala
 *
 * Copyright (C) 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


using GXml;

public errordomain Girp.DocumentError {
    GIR_FILE_INVALID_ERROR
}

public class Girp.Document : GXml.Document
{
    [Description(nick="::ROOT")]
	public Repository repository { get; set; }
	construct {
	    repository = GLib.Object.new (typeof (Repository), "owner-document", this) as Repository;
	}
	public void read (GLib.File file) throws GLib.Error {
	    if (!file.query_exists ()) {
	        throw new Girp.DocumentError.GIR_FILE_INVALID_ERROR ("Give GIR file doesn't exits: %s", file.get_uri ());
	    }
	    if (!file.get_basename ().has_suffix (".gir")) {
	        throw new Girp.DocumentError.GIR_FILE_INVALID_ERROR ("File is not GIR: %s", file.get_uri ());
	    }
		read_from_file (file);
	}
}
