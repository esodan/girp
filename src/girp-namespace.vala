/* girp-namespace.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
using GXml;

public class Girp.Namespace : Element, Named, Documented
{
  [Description (nick="::name")]
  public string name { get; set; }
  [Description (nick="::version")]
  public string version { get; set; }
  [Description (nick="::c:prefix")]
  public string cprefix { get; set; }
  public Doc doc { get; set; }
  Record.Map _records;
  public Record.Map records {
    get {
      if (_records == null)
        set_instance_property ("records");
      return _records;
    }
    set {
      if (_records != null) {
        try {
          clean_property_elements ("records");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _records = value;
    }
  }
  Class.Map _classes;
  public Class.Map classes {
    get {
      if (_classes == null)
        set_instance_property ("classes");
      return _classes;
    }
    set {
      if (_classes != null) {
        try {
          clean_property_elements ("classes");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _classes = value;
    }
  }
  Interface.Map _interfaces;
  public Interface.Map interfaces {
    get {
      if (_interfaces == null)
        set_instance_property ("interfaces");
      return _interfaces;
    }
    set {
      if (_interfaces != null) {
        try {
          clean_property_elements ("interfaces");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _interfaces = value;
    }
  }
  Function.Map _functions;
  public Function.Map functions {
    get {
      if (_functions == null)
        set_instance_property ("functions");
      return _functions;
    }
    set {
      if (_functions != null) {
        try {
          clean_property_elements ("functions");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _functions = value;
    }
  }
  construct {
    try {
      initialize ("namespace");
      _classes = new Class.Map ();
      _classes.initialize_element (this);
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
}
