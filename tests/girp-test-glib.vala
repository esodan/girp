/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-test.vala

 * Copyright (C) 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Girp;
using GXml;

class GirpTest.Suite : GLib.Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/girp/repository/gio",
    ()=>{
      try {
        var gir_doc = new Girp.Document ();
        var dir = GLib.File.new_for_path (TestConfig.SRC_DIR);
        var f = GLib.File.new_for_uri (dir.get_uri () + "/GLib-2.0.gir");
        message (f.get_path ());
        assert (f.query_exists ());
        gir_doc.read (f);
        message ("Test read file");
        assert (gir_doc.repository != null);
        var gir = gir_doc.repository;
        assert (gir != null);
        assert (gir.ns != null);
        assert (gir.ns.name == "GLib");
        assert (gir.ns.version == "2.0");
        assert (gir.ns.records != null);
        message ("Records: %d", gir.ns.records.length);
        assert (gir.ns.records.length > 0);
        foreach (DomElement e in gir.ns.records) {
            assert (e is Girp.Record);
            var r = e as Girp.Record;
            message ("Record: %s", r.name);
        }
        assert (gir.ns.functions != null);
        assert (gir.ns.functions.length > 0);
        var c1 = gir.ns.records.get_item (0) as Girp.Record;
        assert (c1 != null);
        message (c1.name);
        assert (c1.doc != null);
        var app = gir.ns.records.get ("Array") as Girp.Record;
        assert (app != null);
        assert (app.functions.length > 0);
        var f1 = app.functions.get_item (0) as Girp.Function;
        assert (f1 != null);
        message (app.name+"."+f1.name);
        message (f1.doc.text);
        assert (f1.doc.text != "");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
